//////////////////////////////////////////////////////////////////////////////
///          University of Hawaii, College of Engineering
/// @brief   Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file    hello2.cpp
/// @version 1.0 - Initial implementation
///
/// Output Hello World! without using namespace std
///
/// @author  Baishen Wang <baishen@hawaii.edu>
/// @@date   23_Feb_2022
///
///
//////////////////////////////////////////////////////////////////////////////
int main() {
   std::cout<<"Hello World!"<<std::endl;
   return 0;

}

